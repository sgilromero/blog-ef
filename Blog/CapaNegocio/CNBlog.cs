﻿using CapaDatos.Metodos;
using CapaNegocio.Auxiliares;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CapaNegocio
{
    public class CNBlog
    {
        public static List<CNArticulo> ObtenerArticulos()
        {
            try
            {
                var capa_datos = EF.ObtenerArticulos();

                return capa_datos.Select(x => new CNArticulo
                {
                    IdArticulo = x.IdArticulo,
                    Titulo = x.Titulo,
                    Descripcion = x.Descripcion,
                    Usuario = x.CreadoPor.Nombre,
                    Fecha = x.Fecha
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<CNUsuario> ObtenerUsuarios()
        {
            try
            {
                var capa_datos = EF.ObtenerUsuarios();

                return capa_datos.Select(x => new CNUsuario
                {
                    IdUsuario = x.IdUsuario,
                    Nombre = x.Nombre
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AgregarArticulo(string Titulo, string Descripcion, int IdUsuario)
        {
            try
            {
                var capa_datos = EF.AgregarArticulo(Titulo, Descripcion, IdUsuario);

                if (capa_datos)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
