﻿using System;

namespace CapaNegocio.Auxiliares
{
    public class CNArticulo
    {
        public int IdArticulo { get; set; }

        public string Titulo { get; set; }

        public string Descripcion { get; set; }

        public string Usuario { get; set; }

        public DateTime Fecha { get; set; }
    }
}
