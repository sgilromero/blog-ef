namespace CapaDatos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articulos",
                c => new
                    {
                        Id_Articulo = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false, maxLength: 50),
                        Descripcion = c.String(nullable: false, maxLength: 250),
                        FK_Id_Usuario = c.Int(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id_Articulo)
                .ForeignKey("dbo.Usuarios", t => t.FK_Id_Usuario, cascadeDelete: false)
                .Index(t => t.FK_Id_Usuario);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id_Usuario = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id_Usuario);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articulos", "FK_Id_Usuario", "dbo.Usuarios");
            DropIndex("dbo.Articulos", new[] { "FK_Id_Usuario" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.Articulos");
        }
    }
}
