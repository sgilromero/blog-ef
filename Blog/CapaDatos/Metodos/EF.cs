﻿using CapaDatos.Tablas;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CapaDatos.Metodos
{
    public class EF
    {
        public static List<Articulo> ObtenerArticulos()
        {
            try
            {
                using (var context = new Blog())
                {
                    return context.Articulos.Include("CreadoPor").ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Usuario> ObtenerUsuarios()
        {
            try
            {
                using (var context = new Blog())
                {
                    return context.Usuarios.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool AgregarArticulo(string Titulo, string Descripcion, int IdUsuario)
        {
            try
            {
                using (var context = new Blog())
                {
                    var nuevo = new Articulo
                    {
                        Titulo = Titulo,
                        Descripcion = Descripcion,
                        IdUsuario = IdUsuario,
                        Fecha = DateTime.Now
                    };

                    context.Articulos.Add(nuevo);
                    
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }        
    }
}
