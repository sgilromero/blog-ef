﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapaDatos.Tablas
{
    [Table("Articulos")]
    public class Articulo
    {
        public Articulo()
        {

        }

        [Key]
        [Column("Id_Articulo", Order = 1)]
        public int IdArticulo { get; set; }

        [Required, MaxLength(50)]
        [Column("Titulo", Order = 2)]
        public string Titulo { get; set; }

        [Required, MaxLength(250)]
        [Column("Descripcion", Order = 3)]
        public string Descripcion { get; set; }

        [ForeignKey("CreadoPor")]
        [Column("FK_Id_Usuario", Order = 4)]
        public int IdUsuario { get; set; }
        
        [Column("Fecha", Order = 5)]
        public DateTime Fecha { get; set; }

        [Column("FechaModificacion", Order = 6)]
        public DateTime FechaModificacion { get; set; }

        //Relationship
        public virtual Usuario CreadoPor { get; set; }
    }
}
