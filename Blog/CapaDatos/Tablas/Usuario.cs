﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapaDatos.Tablas
{
    [Table("Usuarios")]
    public class Usuario
    {
        public Usuario()
        {

        }

        [Key]
        [Column("Id_Usuario", Order = 1)]
        public int IdUsuario { get; set; }

        [Required, MaxLength(50)]
        [Column("Nombre", Order = 2)]
        public string Nombre { get; set; }
    }
}
