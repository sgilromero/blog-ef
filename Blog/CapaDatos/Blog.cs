﻿using CapaDatos.Tablas;
using System.Data.Entity;

namespace CapaDatos
{
    public class Blog : DbContext
    {
        public Blog() : base("name=Blog")
        {
            var ensureDLLIsCopied =
               System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Articulo> Articulos { get; set; }
    }
}
