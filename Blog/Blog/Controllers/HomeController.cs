﻿using Blog.Models;
using CapaNegocio;
using CapaNegocio.Auxiliares;
using System.Linq;
using System.Web.Mvc;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var articulos = CNBlog.ObtenerArticulos();

            var usuarios = CNBlog.ObtenerUsuarios();

            var modelo = new ArticulosModel
            {
                Articulos = articulos.Select(x => new CNArticulo {
                     IdArticulo = x.IdArticulo,
                     Titulo = x.Titulo,
                     Descripcion = x.Descripcion,
                     Usuario = x.Usuario,
                     Fecha = x.Fecha
                }).ToList(),
                Usuarios = new SelectList(usuarios.Select(y => new SelectListItem {
                    Value = y.IdUsuario.ToString(),
                    Text = y.Nombre
                }), "Value", "Text")
            };

            return View(modelo);
        }

        [HttpPost]
        public void Create(int Id, string Titulo, string Descripcion)
        {
            var respuesta = CNBlog.AgregarArticulo(Titulo, Descripcion, Id);
        }
    }
}