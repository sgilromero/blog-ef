﻿using CapaNegocio.Auxiliares;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Blog.Models
{
    public class ArticulosModel
    {
        public List<CNArticulo> Articulos { get; set; }

        public SelectList Usuarios { get; set; }
    }
}

